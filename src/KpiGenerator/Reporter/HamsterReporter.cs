﻿using Coscine.Configuration;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using System;
using System.Collections.Generic;

namespace Coscine.KpiGenerator.Reporter
{
    public class HamsterReporter
    {
        private readonly RestClient _restClient;
        private readonly string _hamsterUrl = @"http://datahamster.itc.rwth-aachen.de/put.php";

        private readonly string _name = "messdaten_lieferant";
        private readonly string _password = "";
        private readonly string _liveMode = "0";

        public HamsterReporter(IConfiguration configuration)
        {
            _password = configuration.GetStringAndWait("coscine/global/reporting/password");
            _liveMode = configuration.GetStringAndWait("coscine/global/reporting/live");
            _restClient = new RestClient();
        }

        public void Send(List<Kpi> kpis)
        {
            // when all generators are done
            var kpiModel = new KpiModel();
            foreach (var kpi in kpis)
            {
                var values = $"login={_name}&password={_password}";
                values += FormatSingleKpi(kpi);
                if (_liveMode != "1")
                {
                    values += "&debug=1";
                }

                var result = SendData(values);
                // If everything works, output should be empty
                if (result.Trim() != "")
                {
                    Console.Error.WriteLine(result);
                }
                else
                {
                    kpiModel.MarkAsSent(kpi);
                }
            }
        }

        private string SendData(string data)
        {
            return _restClient.HttpPost(new Uri(_hamsterUrl), data, null);
        }

        private string FormatSingleKpi(Kpi kpi)
        {
            return
                $"&messgroesse_id={kpi.MeasurementID}" +
                $"&wert={kpi.Value}" +
                $"&start={FormatDate(kpi.Start)}" +
                $"&ende={FormatDate(kpi.End)}" +
                $"&ikz={kpi.Ikz ?? ""}" +
                $"&zusatzinfo={kpi.AdditionalInfo ?? ""}" +
                $"&zusatzinfo1={kpi.AdditionalInfo1 ?? ""}" +
                $"&zusatzinfo2={kpi.AdditionalInfo2 ?? ""}" +
                $"&zusatzinfo3={kpi.AdditionalInfo3 ?? ""}" +
                $"&zusatzinfo4={kpi.AdditionalInfo4 ?? ""}" +
                $"&zusatzinfo5={kpi.AdditionalInfo5 ?? ""}";
        }

        private string FormatDate(DateTime? date)
        {
            if (!date.HasValue)
            {
                return "";
            }
            else
            {
                return ((DateTime)date).ToString("yyyy-MM-dd-HH-mm-ss");
            }
        }
    }
}
