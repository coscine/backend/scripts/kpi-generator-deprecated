﻿using Coscine.Configuration;
using Coscine.Database.DataModel;
using System;
using System.Collections.Generic;

namespace Coscine.KpiGenerator
{
    public abstract class KpiGenerator
    {
        protected IConfiguration _configuration;
        protected string _measurementID;

        public KpiGenerator(IConfiguration configuration, string measurementID)
        {
            _configuration = configuration;
            _measurementID = measurementID;
        }

        public abstract List<Kpi> GenerateKpis();

        public Kpi CreateKpi(string ikz, double value)
        {
            return new Kpi()
            {
                MeasurementID = _measurementID,
                Ikz = ikz,
                Value = value,
                Start = DateTime.UtcNow
            };
        }
    }
}
