﻿using Coscine.Configuration;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.KpiGenerator.Generators
{
    public class GenerateUsersPerIkz : KpiGenerator
    {
        public GenerateUsersPerIkz(IConfiguration configuration, string measurementID) : base(configuration, measurementID)
        {
        }

        public override List<Kpi> GenerateKpis()
        {
            var externalIdModel = new ExternalIdModel();
            var rdfStoreConnector = new RdfStoreConnector(_configuration.GetStringAndWait("coscine/local/virtuoso/additional/url"));
            var organizationToIkzDict = rdfStoreConnector.GetOrganizationToIkzMap();

            var dictUser = new Dictionary<string, int>();
            var userModel = new UserModel();

            var result = new List<Kpi>();

            var allUsersThatAcceptedTOS = userModel.GetActiveUserIds();

            var countForeignOrganizations = 0;
            var countRawRWTHOrganizations = 0;
            foreach (var userId in allUsersThatAcceptedTOS)
            {
                var externalIds = externalIdModel.GetAllWhere((externalId) => externalId.UserId == userId);
                var externalIdList = new List<string>();
                foreach (var externalId in externalIds)
                {
                    externalIdList.Add(externalId.ExternalIdColumn);
                }
                var memberData = rdfStoreConnector.GetTriples(new Uri("https://ror.org/04xfq0f34"), null, null, 1, externalIdList);

                // data available in virtuoso
                if (memberData.Count() != 0)
                {
                    foreach (var member in memberData)
                    {
                        var subject = member.Subject.ToString();
                        if (dictUser.ContainsKey(subject))
                        {
                            dictUser[subject] = dictUser[subject] + 1;
                        }
                        else
                        {
                            dictUser.Add(subject, 1);
                        }
                    }
                }
                else
                {
                    var user = userModel.GetById(userId);
                    if (user.Institute != null)
                    {
                        if (dictUser.ContainsKey(user.Institute))
                        {
                            dictUser[user.Institute] += 1;
                        }
                        else
                        {
                            dictUser.Add(user.Institute, 1);
                        }
                    }
                }
            }


            foreach (var user in dictUser)
            {
                if (dictUser.ContainsKey("https://ror.org/04xfq0f34"))
                {
                    countRawRWTHOrganizations += dictUser["https://ror.org/04xfq0f34"];
                }
                else if (organizationToIkzDict.ContainsKey(user.Key))
                {
                    result.Add(new Kpi()
                    {
                        MeasurementID = _measurementID,
                        Ikz = organizationToIkzDict[user.Key],
                        Value = dictUser[user.Key],
                        Start = DateTime.UtcNow
                    });
                }
                else
                {
                    countForeignOrganizations += dictUser[user.Key];
                }
            }

            result.Add(CreateKpi("000000", countForeignOrganizations));
            result.Add(CreateKpi("000001", countRawRWTHOrganizations));
            return result;
        }
    }
}
