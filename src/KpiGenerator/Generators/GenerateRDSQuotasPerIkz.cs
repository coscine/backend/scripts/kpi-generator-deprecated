﻿using Coscine.Configuration;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Metadata;
using System.Collections.Generic;

namespace Coscine.KpiGenerator.Generators
{
    public class GenerateRDSQuotasPerIkz : KpiGenerator
    {
        public GenerateRDSQuotasPerIkz(IConfiguration configuration, string measurementID) : base(configuration, measurementID)
        {
        }

        public override List<Kpi> GenerateKpis()
        {
            var rdfStoreConnector = new RdfStoreConnector(_configuration.GetStringAndWait("coscine/local/virtuoso/additional/url"));
            var organizationToIkzDict = rdfStoreConnector.GetOrganizationToIkzMap();
            var rdsResourceTypeModel = new RDSResourceTypeModel();
            var resourceModel = new ResourceModel();

            var result = new List<Kpi>();

            var bucketForProjects = resourceModel.GetRDSResourceListByOrganization();

            var dictQuota = new Dictionary<string, double>();

            var countForeignOrganizations = 0.0;
            var countRawRWTHOrganizations = 0.0;
            foreach (var res in bucketForProjects)
            {
                if (res.Url == null)
                {
                    continue;
                }
                foreach (var r in res.Resources)
                {
                    var rdsResourceType = rdsResourceTypeModel.GetById(resourceModel.GetById(r.Id).ResourceTypeOptionId.Value);
                    var bucketname = rdsResourceType.BucketName;

                    var subject = res.Url;
                    if (dictQuota.ContainsKey(subject))
                    {
                        dictQuota[subject] = dictQuota[subject] + (int)(rdsResourceType.Size);
                    }
                    else if (subject.Contains("https://ror.org/04xfq0f34#"))
                    {
                        dictQuota.Add(subject, (int)(rdsResourceType.Size));
                    }
                    else if (subject == "https://ror.org/04xfq0f34")
                    {
                        countRawRWTHOrganizations += (int)(rdsResourceType.Size);
                    }
                    else
                    {
                        countForeignOrganizations += (int)(rdsResourceType.Size);
                    }
                }
            }

            foreach (var quota in dictQuota)
            {
                if (organizationToIkzDict.ContainsKey(quota.Key))
                {
                    result.Add(CreateKpi(organizationToIkzDict[quota.Key], dictQuota[quota.Key]));
                }
            }
            result.Add(CreateKpi("000000", countForeignOrganizations));
            result.Add(CreateKpi("000001", countRawRWTHOrganizations));
            return result;
        }
    }
}
