﻿using Coscine.Configuration;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Metadata;
using System.Collections.Generic;

namespace Coscine.KpiGenerator.Generators
{
    public class GenerateRDSBucketsPerIkz : KpiGenerator
    {
        public GenerateRDSBucketsPerIkz(IConfiguration configuration, string measurementID) : base(configuration, measurementID)
        {
        }

        public override List<Kpi> GenerateKpis()
        {
            var rdfStoreConnector = new RdfStoreConnector(_configuration.GetStringAndWait("coscine/local/virtuoso/additional/url"));
            var organizationToIkzDict = rdfStoreConnector.GetOrganizationToIkzMap();
            var resourceModel = new ResourceModel();

            var result = new List<Kpi>();

            var bucketForProjects = resourceModel.GetRDSBucketCountByOrganization();

            var countForeignOrganizations = 0;
            var countRawRWTHOrganizations = 0;
            foreach (var r in bucketForProjects)
            {
                if (r.Url == null)
                {
                    continue;
                }
                if (organizationToIkzDict.ContainsKey(r.Url))
                {
                    result.Add(CreateKpi(organizationToIkzDict[r.Url], r.Count));
                }
                else
                {
                    if (r.Url == "https://ror.org/04xfq0f34")
                    {
                        countRawRWTHOrganizations += r.Count;
                    }
                    else
                    {
                        countForeignOrganizations += r.Count;
                    }
                }
            }
            result.Add(CreateKpi("000000", countForeignOrganizations));
            result.Add(CreateKpi("000001", countRawRWTHOrganizations));
            return result;
        }
    }
}
