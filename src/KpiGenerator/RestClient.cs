﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Coscine.KpiGenerator
{
    /// <summary>
    /// This class is a legacy implementation and was adopted to ensure compatibility with the previous DataHamster implementation.
    /// </summary>
    public class RestClient
    {
        private static readonly string _urlTemplateWithQuery = "{0}?{1}";
        private static readonly string _urlTemplateWithoutQuery = "{0}";

        private static WebResponse HttpRequest(
            Uri endpoint,
            string query = null,
            string method = "GET",
            string body = null,
            string user = null,
            string password = null,
            string contentType = "application/json; charset=utf-8",
            string accept = "application/json, */*",
            string userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64)",
            Dictionary<string, string> headers = null
            )
        {
            if (string.IsNullOrWhiteSpace(method))
            {
                method = "GET";
            }
            method = method.ToUpper();

            var template = _urlTemplateWithQuery;
            if (string.IsNullOrWhiteSpace(query))
            {
                template = _urlTemplateWithoutQuery;
                query = string.Empty;
            }

            var request = (HttpWebRequest)WebRequest.Create(string.Format(template, endpoint.OriginalString, query));

            if (headers != null)
            {
                foreach (var keyValuePair in headers)
                {
                    request.Headers.Add(keyValuePair.Key, keyValuePair.Value);
                }
            }

            request.Method = method;
            request.ContentType = contentType;
            request.Accept = accept;
            request.UserAgent = userAgent;

            if (!string.IsNullOrWhiteSpace(user) && !string.IsNullOrWhiteSpace(password))
            {
                request.Credentials = new NetworkCredential(user, password);
                request.PreAuthenticate = true;
            }

            if (!string.IsNullOrWhiteSpace(body) && method != "GET")
            {
                using (var stream = request.GetRequestStream())
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write(body);
                    writer.Flush();
                }
            }
            else
            {
                request.ContentLength = 0;
            }

            WebResponse response = request.GetResponse();
            return response;
        }

        private string ReadResponse(WebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(responseStream))
            {
                string responseFromServer = reader.ReadToEnd();
                return responseFromServer.Trim();
            }
        }


        #region HTTP POST Shortcuts

        public string HttpPost(Uri endpoint, string query, string body = null)
        {
            return Http("POST", endpoint, query, body);
        }

        #endregion

        public string Http(string method, Uri endpoint, string query, string body = null, string user = null, string password = null)
        {
            using (var response = HttpRequest(endpoint, query, method, body, user, password))
            {
                return ReadResponse(response);
            }
        }

    }
}
