﻿using Coscine.Configuration;
using Coscine.Database.Settings;
using Coscine.Database.Models;
using LinqToDB.Data;
using System.Collections.Generic;
using System.Linq;
using Coscine.KpiGenerator.Reporter;
using Coscine.KpiGenerator.Generators;
using Coscine.Database.DataModel;

namespace Coscine.KpiGenerator
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ConsulConfiguration configuration = new ConsulConfiguration();
            DataConnection.DefaultSettings = new CoscineSettings(configuration);

            var liveMode = configuration.GetStringAndWait("coscine/global/reporting/live");

            // Run all Generators for the KPI
            // each Generator returns a list of KPI, it can have zero entries.
            List<Kpi> newKpis = new List<Kpi>();
            newKpis.AddRange(new GenerateProjectsPerIkz(configuration, "2969").GenerateKpis());
            newKpis.AddRange(new GenerateRDSBucketsPerIkz(configuration, "2971").GenerateKpis());
            newKpis.AddRange(new GenerateRDSQuotasPerIkz(configuration, "2973").GenerateKpis());
            newKpis.AddRange(new GenerateUsersPerIkz(configuration, "2967").GenerateKpis());

            // when all generators are done
            var kpiModel = new KpiModel();
            // each value is stored within the database
            foreach (Kpi kpi in newKpis)
            {
                kpiModel.Insert(kpi);
            }

            // get all values from the database that have not been successfully sent yet
            var kpiToProcess = kpiModel.GetAllWhere((kpi) => (kpi.SentSuccessfully == false)).ToList();

            // try to sent them
            HamsterReporter hamsterReporter = new HamsterReporter(configuration);
            if (liveMode == "1")
            {
                hamsterReporter.Send(kpiToProcess);
            }
        }
    }
}
