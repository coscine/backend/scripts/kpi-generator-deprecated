## ProxyApi

This Api can access the RWTHAppProxy and OAuth2 of the RWTH Aachen University.

Furthermore some helper classes exists which can verify tokens for accessing certain Api methods.

The following configuration keys are currently settable:

* "coscine/global/context/metadata/scope"
* "coscine/global/context/metadata/serviceid"
* "coscine/global/context/coscine/scope"
* "coscine/global/context/coscine/serviceid"
* "coscine/local/proxytest/refreshtoken" <= A valid refresh token for testing
* "coscine/global/epic/prefix"
* "coscine/global/epic/url"
* "coscine/global/epic/user"
* "coscine/global/epic/password"

## Building

Build this project by running either the build.ps1 or the build<span></span>.sh script.
The project will be build and tested.

### Links 

*  [Commit convention](docs/ESLintConvention.md)
*  [Everything possible with markup](docs/testdoc.md)
*  [Adding NUnit tests](docs/nunit.md)